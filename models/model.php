<?php

use Silex\Application;
use Silex\ServiceProviderInterface;


/*
CREATE TABLE `silex_test`.`user` (
  `id` INT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `pass` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `silex_test`.`lobbys` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lobby_col` INT NULL,
  `master` INT NULL,
  `status` VARCHAR(45) NULL,
  `lobby_active_users` INT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `silex_test`.`lobbys` 
ADD COLUMN `master_name` VARCHAR(255) NULL AFTER `lobby_active_users`;


*/

class Model {
 	public $app;
    public function __construct(Application $app) {
        $this->app = $app;
    }
    public function index() {
    	
    $sql = "SELECT * FROM lobbys";
    $messages = $this->app['dbs']['mysql_read']->fetchAll($sql);



        return $messages;
    }
 	
 	public function register() {
    

	$name = (strip_tags($_POST['name']));
	$password = (strip_tags($_POST['password'])); 
    $this->app['dbs']['mysql_write']->insert('user', array
    (
    'name' =>  $name,
	'pass' => $password
    ));

      

        return 1;
    }

    public function create_lobby_2() {
    

    $this->app['dbs']['mysql_write']->insert('lobbys', array
    (
    'lobby_col' =>  2,
    'master' => $_SESSION['id'],
    'status' => 1,
    'lobby_active_users' => 1,
    'master_name' => $_SESSION['name']
    ));

        return 1;
    }

    public function create_lobby_4() {
    

    $this->app['dbs']['mysql_write']->insert('lobbys', array
    (
    'lobby_col' =>  4,
    'master' => $_SESSION['id'],
    'status' => 1,
    'lobby_active_users' => 1,
    'master_name' => $_SESSION['name']
    ));

        return 1;
    }
	public function like($id) {
     
    $sql = "UPDATE silex_test SET likes = likes + 1 WHERE id = ?";
    $this->app['dbs']['mysql_write']->executeUpdate($sql, array( (int) $id));
	$sql = "SELECT * FROM silex_test WHERE id = ?";
    $row = $this->app['dbs']['mysql_read']->fetchAssoc($sql, array((int) $id));
    	echo "Нравится ".$row['likes'];
       
    }

    public function login() {
    	
    $sql = "SELECT id, name FROM user WHERE name = ? AND pass = ?";
    $message = $this->app['dbs']['mysql_read']->fetchAssoc($sql, array($_POST['name'], $_POST['password']));



        return $message;
    }
}

