<?php 
$title = "Сетевая игра";
$subtitle = "Предлагаем поиграть!";
$register = "Регистрация";
$login = "Вход";
$name = "Имя";
$password = "Пароль";
$register_ok = "Успешная регистрация";
$register_fail = "Заполните поля";
$login_fail = "Вы не вошли";
$hello = "Привет";
$logout = "Выход";
$create_lobby_2 = "Создать лобби на 2-х игроков";
$create_lobby_4 = "Создать лобби на 4-х игроков";
$lobby_text = "Лобби на ";
$players = "игроков";
$master_name = "Мастер ";
$col_players = "Колличество подключённых игроков";
$join = "Подключиться";
$out = "Выйти";