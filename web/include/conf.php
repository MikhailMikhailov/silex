<?php
$location = '/silex/web/';


$lang = 'ru';

if(!isset($_SESSION['lang'])){
   $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

   if($lang != 'ru'){
        $lang = 'en';
   }

}else{
	$lang = $_SESSION['lang'];
}
require_once __DIR__.'/'.$lang.'.php';