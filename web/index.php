
<?php

require_once __DIR__.'/../vendor/autoload.php';

session_start();

$app = new Silex\Application();
//$app['debug'] = true;
//print_r($_SESSION);

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


require_once __DIR__.'/../models/DB.php';

require_once __DIR__.'/../models/main.php';


$app->register(new Providers\ModelsServiceProvider(), array(
    'models.path' => __DIR__ . '/../models/'
));


$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));

$app->get('/', function() use ($app) {
    require_once __DIR__.'/include/conf.php';
    $lobbys =  $app['models']->load('model', 'index');
    $name_user = "";
    if(isset($_SESSION['name'])){
        $name_user = $_SESSION['name'];
    }
    $message = "";
    if(isset($_SESSION['message'])){
        $message = $_SESSION['message'];
    }
 	$out = $app['twig']->render('index.html.php', array(
        'title' => $title,
        'subtitle' => $subtitle,
        'location' => $location,
        'register' => $register,
        'login' => $login,
        'name' => $name,
        'password' => $password,
        'message' => $message,
        'name_user' => $name_user,
        'hello' => $hello,
        'logout' => $logout,
        'create_lobby_2' => $create_lobby_2,
        'create_lobby_4' => $create_lobby_4,
        'success' => "",
        'lobbys' => $lobbys,
        'lobby_text' => $lobby_text,
        'players' => $players,
        'master_name' => $master_name,
        'col_players' => $col_players,
        'join' => $join,
        'out' => $out

    ));
 	$_SESSION['message'] = "";


    return $out;

   

});



$app->match('/subscribe', function() use ($app) {
    header('Content-Type', 'text/plain;charset=utf-8');
    header("Cache-Control", "no-cache, must-revalidate");
set_time_limit(5);
while(1==1){
    //
}

    return "";
});



$app->get('/ru', function() use ($app) {
    require_once __DIR__.'/include/conf.php';
 	$_SESSION['lang'] = 'ru';
    $_SESSION['message'] = "";
    header('Location: '.$location);
    exit;

   
});


$app->get('/en', function() use ($app) {
    require_once __DIR__.'/include/conf.php';
    $_SESSION['lang'] = 'en';
    $_SESSION['message'] = "";
    header('Location: '.$location);
    exit;
   
});


$app->match('/register', function() use ($app) {
    require_once __DIR__.'/include/conf.php';

    if(!empty($_POST['name']) && !empty($_POST['password'])){
        $app['models']->load('model', 'register');
        $_SESSION['message'] = $register_ok;
    }else{
        $_SESSION['message'] = $register_fail;
    }
    
     header('Location: '.$location);
    exit;

   
});


$app->match('/login', function() use ($app) {
    require_once __DIR__.'/include/conf.php';

    if(!empty($_POST['name']) && !empty($_POST['password'])){
            $login_stack = $app['models']->load('model', 'login');
        if($login_stack){
            $_SESSION['name'] = $login_stack['name'];
            $_SESSION['id'] = $login_stack['id'];
        }else{
             $_SESSION['message'] = $login_fail;
        }

    }else{
        $_SESSION['message'] = $login_fail;
    }
    
     header('Location: '.$location);
    exit;

   
});



$app->get('/logout', function() use ($app) {
    require_once __DIR__.'/include/conf.php';
    $_SESSION['name'] = '';
    header('Location: '.$location);
    exit;

   
});


$app->get('/createlobby2', function() use ($app) {
    require_once __DIR__.'/include/conf.php';
    $app['models']->load('model', 'create_lobby_2');
    header('Location: '.$location);
    exit;

   
});


$app->get('/createlobby4', function() use ($app) {
   require_once __DIR__.'/include/conf.php';

    $app['models']->load('model', 'create_lobby_4');
    header('Location: '.$location);
    exit;

   
});

$app->run();
